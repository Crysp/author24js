/**
 * Показывает попап слайдера
 *
 * @param {Number} slideIndex - индекс текущего слайда
 */
Slider.prototype.showSlider = function (slideIndex) {

    // если нет переменной $base, значит попапа нет в доме и его надо нарисовать
    if (typeof this.$base === 'undefined') {
        this.buildPopup(slideIndex);
    // иначе надо показать выбранную картинку
    } else {
        var selectedSlide = this.ui.slides[slideIndex];
        this.ui.slides.removeClass('active').hide();
        this.$currentSlide = $(selectedSlide).show();
        this.showViewport();
    }

    this.$base.show();
};

/**
 * Прячет попап
 */
Slider.prototype.hideSlider = function () {
    this.$base.hide();
};

/**
 * Показывает следующий слайд
 */
Slider.prototype.showNext = function () {

    // ищу следующий слайд, если его нет, то беру первый
    var $nextSlide = this.$currentSlide.next().length ? this.$currentSlide.next() : this.ui.slides.first();
    var $moveGroup = _.clone(this.$currentSlide);

    // создается группа из текущего и следующего слайда, для удобной анимации
    $moveGroup.push($nextSlide[0]);

    // подготовка следующего слайда для анимации
    $nextSlide.css({
        left: parseInt(this.$currentSlide.css('left')) + _size.width
    }).show();

    this._isSliding = true;

    // спецэффект
    $moveGroup.animate({
        left: '-=' + _size.width
    }, 200, function () {
        // если оба слайда не двигаются, можно поменять значение текущего слайда в плагине
        if (!this.$currentSlide.is(':animated') && !$nextSlide.is(':animated')) {
            this._isSliding = false;
            this.$currentSlide.removeClass('active').hide();
            this.$currentSlide = $nextSlide;
            this.$currentSlide.addClass('active');
        }
    }.bind(this));
};

/**
 * Показывает предыдущий слайд (по аналогии с показом следующего)
 */
Slider.prototype.showPrev = function () {

    var $prevSlide = this.$currentSlide.prev().length ? this.$currentSlide.prev() : this.ui.slides.last();
    var $moveGroup = _.clone(this.$currentSlide);

    $moveGroup.push($prevSlide[0]);

    $prevSlide.css({
        left: parseInt(this.$currentSlide.css('left')) - _size.width
    }).show();

    this._isSliding = true;

    $moveGroup.animate({
        left: '+=' + _size.width
    }, 200, function () {
        if (!this.$currentSlide.is(':animated') && !$prevSlide.is(':animated')) {
            this._isSliding = false;
            this.$currentSlide.removeClass('active').hide();
            this.$currentSlide = $prevSlide;
            this.$currentSlide.addClass('active');
        }
    }.bind(this));
};

/**
 * Выравнивает элементы слайдера.
 * Используется, когда меняются размеры окна, слайда и т.д.
 */
Slider.prototype.showViewport = function () {
    var $prevSlide = this.$currentSlide.prev();
    var $nextSlide = this.$currentSlide.next();

    if ($prevSlide.length) {
        $prevSlide.css({
            left: -_size.width
        });
    }

    if ($nextSlide.length) {
        $nextSlide.css({
            left: +_size.width
        });
    }

    this.$currentSlide.css({
        left: 0
    });
};

/**
 * Размеры окна поменялись
 */
Slider.prototype.onWindowResize = function () {
    _size.width = $(window).width() - sideOffset * 2;
    _size.height = $(window).height() - sideOffset * 2;

    // если попап есть в доме, значит надо редактировать размеры слайдов
    if (typeof this.ui !== 'undefined') {

        this.showViewport();

        this.ui.container.width(_size.width);
        this.ui.container.height(_size.height);

        this.ui.slides.find('img').each(function (index, image) {

            var $image = $(image);
            var size = proportionalSize($image.height(), $image.width());

            if (size.height > _size.height) {
                $image.height('100%');
                $image.width('auto');
            } else {
                $image.width('100%');
                $image.height('auto');
            }

        });
    }
};