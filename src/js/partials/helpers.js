/**
 * Расчет пропорциональных размеров картинки относительно области показа картинки
 *
 * @param {Number} naturalWidth - исходная ширина
 * @param {Number} naturalHeight - исходная высота
 * @returns {Object} - расчитанные размеры
 */
function proportionalSize(naturalWidth, naturalHeight) {
    return {
        width: _size.width,
        height: (naturalWidth / naturalHeight) * _size.width
    };
}