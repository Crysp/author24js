/**
 * Базовая настройка плагина
 */
Slider.prototype.init = function () {

    var $images = this.$el.find('img');

    // сохранение ссылок на картинки, которые надо показать
    $images.each(function (index, image) {
        this.images.push($(image).data('src'));
    }.bind(this));

    // отлавливает клик по картинке, чтобы открыть слайдер
    $(_handleClass).click(function (e) {
        // в массиве картинок ищу индекс картинки которую надо показать
        var index = _.indexOf(this.images, $($(e.currentTarget).find('img')[0]).data('src'));
        this.showSlider(index);
    }.bind(this));
};

/**
 * Добавляет попап слайдера в дом
 *
 * @param {Number} slideIndex - индекс картинки которую надо изначально показать
 */
Slider.prototype.buildPopup = function (slideIndex) {

    this.template = _.template(_template);

    $('body').append(this.template({
        width: _size.width,
        height: _size.height,
        images: this.images,
        selected: slideIndex
    }));

    this.bindUi();

    this.delegateEvents();
};

/**
 * Сохранение в объекте UI всех элементов с которыми придется работать
 */
Slider.prototype.bindUi = function () {

    this.$base = $(_baseElement);

    this.ui = _.clone(_ui);

    for (var element in this.ui) {
        if(this.ui.hasOwnProperty(element)) {
            this.ui[element] = this.$base.find(this.ui[element]);
        }
    }
};

/**
 * Установка обработчиков событий
 */
Slider.prototype.delegateEvents = function () {

    this.ui.closeBtn.click(function () {
        this.hideSlider();
    }.bind(this));

    this.ui.prevBtn.click(function () {
        if (!this._isSliding) {
            this.showPrev();
        }
    }.bind(this));

    this.ui.nextBtn.click(function () {
        if (!this._isSliding) {
            this.showNext();
        }
    }.bind(this));

    // если картинка загрузилась, значит можно ее показать и поправить размеры под окно
    this.ui.slides.find('img').load(function (e) {

        var $slide = $(e.target);
        var slideWidth = $slide.width();
        var slideHeight = $slide.height();
        var size = proportionalSize(slideWidth, slideHeight);

        if (size.height > _size.height) {
            $slide.width('100%');
        } else {
            $slide.height('100%');
        }

        $slide.parent().css({
            visibility: 'visible'
        }).hide();

        if ($slide.parent().hasClass('active')) {
            this.ui.loading.hide();
            this.$currentSlide = $slide.parent();
            this.$currentSlide.show();
            this.showViewport();
        }
    }.bind(this));

    $(this.$base).on('swipeleft', function () {
        if (!this._isSliding) {
            this.showNext();
        }
    }.bind(this));

    $(this.$base).on('swiperight', function () {
        if (!this._isSliding) {
            this.showPrev();
        }
    }.bind(this));
};