
// элемент у которого есть этот класс, открывает попап
var _handleClass = '.js-thumb';

// отступ слайдов от краев окна
var sideOffset = 50;

// размеры контейнера для слайда
var _size = {
    width: $(window).width() - (sideOffset * 2),
    height: $(window).height() - (sideOffset * 2)
};

// шаблон попапа слайдера
var _template = '' +
    '<div id="slider" class="slider-popup" style="display: none;">' +
        '<button class="close-btn js-close-btn">&#10006;</button>' +
        '<button class="slide-btn mod-left js-prev-btn">&#10096;</button>' +
        '<button class="slide-btn mod-right js-next-btn">&#10097;</button>' +
        '<div class="slider-popup-loading js-loading">Загрузка...</div>' +
        '<div class="slider-popup-container js-container" style="width: <%= width %>px; height: <%= height %>px;">' +
            '<% for (var i in images) { %>' +
            '<div class="slider-popup-thumb js-slide <%- selected == i ? \'active\' : \'\' %>" style="visibility: hidden; left: <%- i * ' + _size.width + ' %>px;"><img src="<%= images[i] %>"></div>' +
            '<% } %>' +
        '</div>' +
    '</div>';

// айдишник попапа слайдера
var _baseElement = '#slider';

// классы элементов управления слайдером
var _ui = {
    container: '.js-container',
    loading: '.js-loading',
    closeBtn: '.js-close-btn',
    prevBtn: '.js-prev-btn',
    nextBtn: '.js-next-btn',
    slides: '.js-slide'
};