(function (factory) {

    if (typeof define === 'function' && define.amd) {

        // AMD. Register as an anonymous module.
        define(['jquery', 'jquery-swipe-event', 'underscore'], factory);
    } else if (typeof exports === 'object') {

        // Node/CommonJS
        factory(require('jquery'), require('jquery-swipe-event'), require('underscore'));
    } else {

        // Browser globals
        factory(jQuery, _);
    }

}(function ($, _) {

    //= partials/variables

    //= partials/contructor

    //= partials/helpers

    //= partials/build

    //= partials/view-actions

    $.fn.slider = function() {

        var slider = new Slider(this);

        $(window).resize(_.debounce(function () {
            Slider.prototype.onWindowResize.call(slider, arguments);
        }, 500));

        return slider;
    };

}));