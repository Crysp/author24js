(function (factory) {

    if (typeof define === 'function' && define.amd) {

        // AMD. Register as an anonymous module.
        define(['jquery', 'jquery-swipe-event', 'underscore'], factory);
    } else if (typeof exports === 'object') {

        // Node/CommonJS
        factory(require('jquery'), require('jquery-swipe-event'), require('underscore'));
    } else {

        // Browser globals
        factory(jQuery, _);
    }

}(function ($, _) {

    
    // элемент у которого есть этот класс, открывает попап
    var _handleClass = '.js-thumb';
    
    // отступ слайдов от краев окна
    var sideOffset = 50;
    
    // размеры контейнера для слайда
    var _size = {
        width: $(window).width() - (sideOffset * 2),
        height: $(window).height() - (sideOffset * 2)
    };
    
    // шаблон попапа слайдера
    var _template = '' +
        '<div id="slider" class="slider-popup" style="display: none;">' +
            '<button class="close-btn js-close-btn">&#10006;</button>' +
            '<button class="slide-btn mod-left js-prev-btn">&#10096;</button>' +
            '<button class="slide-btn mod-right js-next-btn">&#10097;</button>' +
            '<div class="slider-popup-loading js-loading">Загрузка...</div>' +
            '<div class="slider-popup-container js-container" style="width: <%= width %>px; height: <%= height %>px;">' +
                '<% for (var i in images) { %>' +
                '<div class="slider-popup-thumb js-slide <%- selected == i ? \'active\' : \'\' %>" style="visibility: hidden; left: <%- i * ' + _size.width + ' %>px;"><img src="<%= images[i] %>"></div>' +
                '<% } %>' +
            '</div>' +
        '</div>';
    
    // айдишник попапа слайдера
    var _baseElement = '#slider';
    
    // классы элементов управления слайдером
    var _ui = {
        container: '.js-container',
        loading: '.js-loading',
        closeBtn: '.js-close-btn',
        prevBtn: '.js-prev-btn',
        nextBtn: '.js-next-btn',
        slides: '.js-slide'
    };

    function Slider(element) {
    
        this.$el = element;
    
        this.images = [];
    
        this.$currentSlide = null;
        this._isSliding = false;
    
        this.init();
    }

    /**
     * Расчет пропорциональных размеров картинки относительно области показа картинки
     *
     * @param {Number} naturalWidth - исходная ширина
     * @param {Number} naturalHeight - исходная высота
     * @returns {Object} - расчитанные размеры
     */
    function proportionalSize(naturalWidth, naturalHeight) {
        return {
            width: _size.width,
            height: (naturalWidth / naturalHeight) * _size.width
        };
    }

    /**
     * Базовая настройка плагина
     */
    Slider.prototype.init = function () {
    
        var $images = this.$el.find('img');
    
        // сохранение ссылок на картинки, которые надо показать
        $images.each(function (index, image) {
            this.images.push($(image).data('src'));
        }.bind(this));
    
        // отлавливает клик по картинке, чтобы открыть слайдер
        $(_handleClass).click(function (e) {
            // в массиве картинок ищу индекс картинки которую надо показать
            var index = _.indexOf(this.images, $($(e.currentTarget).find('img')[0]).data('src'));
            this.showSlider(index);
        }.bind(this));
    };
    
    /**
     * Добавляет попап слайдера в дом
     *
     * @param {Number} slideIndex - индекс картинки которую надо изначально показать
     */
    Slider.prototype.buildPopup = function (slideIndex) {
    
        this.template = _.template(_template);
    
        $('body').append(this.template({
            width: _size.width,
            height: _size.height,
            images: this.images,
            selected: slideIndex
        }));
    
        this.bindUi();
    
        this.delegateEvents();
    };
    
    /**
     * Сохранение в объекте UI всех элементов с которыми придется работать
     */
    Slider.prototype.bindUi = function () {
    
        this.$base = $(_baseElement);
    
        this.ui = _.clone(_ui);
    
        for (var element in this.ui) {
            if(this.ui.hasOwnProperty(element)) {
                this.ui[element] = this.$base.find(this.ui[element]);
            }
        }
    };
    
    /**
     * Установка обработчиков событий
     */
    Slider.prototype.delegateEvents = function () {
    
        this.ui.closeBtn.click(function () {
            this.hideSlider();
        }.bind(this));
    
        this.ui.prevBtn.click(function () {
            if (!this._isSliding) {
                this.showPrev();
            }
        }.bind(this));
    
        this.ui.nextBtn.click(function () {
            if (!this._isSliding) {
                this.showNext();
            }
        }.bind(this));
    
        // если картинка загрузилась, значит можно ее показать и поправить размеры под окно
        this.ui.slides.find('img').load(function (e) {
    
            var $slide = $(e.target);
            var slideWidth = $slide.width();
            var slideHeight = $slide.height();
            var size = proportionalSize(slideWidth, slideHeight);
    
            if (size.height > _size.height) {
                $slide.width('100%');
            } else {
                $slide.height('100%');
            }
    
            $slide.parent().css({
                visibility: 'visible'
            }).hide();
    
            if ($slide.parent().hasClass('active')) {
                this.ui.loading.hide();
                this.$currentSlide = $slide.parent();
                this.$currentSlide.show();
                this.showViewport();
            }
        }.bind(this));
    
        $(this.$base).on('swipeleft', function () {
            if (!this._isSliding) {
                this.showNext();
            }
        }.bind(this));
    
        $(this.$base).on('swiperight', function () {
            if (!this._isSliding) {
                this.showPrev();
            }
        }.bind(this));
    };

    /**
     * Показывает попап слайдера
     *
     * @param {Number} slideIndex - индекс текущего слайда
     */
    Slider.prototype.showSlider = function (slideIndex) {
    
        // если нет переменной $base, значит попапа нет в доме и его надо нарисовать
        if (typeof this.$base === 'undefined') {
            this.buildPopup(slideIndex);
        // иначе надо показать выбранную картинку
        } else {
            var selectedSlide = this.ui.slides[slideIndex];
            this.ui.slides.removeClass('active').hide();
            this.$currentSlide = $(selectedSlide).show();
            this.showViewport();
        }
    
        this.$base.show();
    };
    
    /**
     * Прячет попап
     */
    Slider.prototype.hideSlider = function () {
        this.$base.hide();
    };
    
    /**
     * Показывает следующий слайд
     */
    Slider.prototype.showNext = function () {
    
        // ищу следующий слайд, если его нет, то беру первый
        var $nextSlide = this.$currentSlide.next().length ? this.$currentSlide.next() : this.ui.slides.first();
        var $moveGroup = _.clone(this.$currentSlide);
    
        // создается группа из текущего и следующего слайда, для удобной анимации
        $moveGroup.push($nextSlide[0]);
    
        // подготовка следующего слайда для анимации
        $nextSlide.css({
            left: parseInt(this.$currentSlide.css('left')) + _size.width
        }).show();
    
        this._isSliding = true;
    
        // спецэффект
        $moveGroup.animate({
            left: '-=' + _size.width
        }, 200, function () {
            // если оба слайда не двигаются, можно поменять значение текущего слайда в плагине
            if (!this.$currentSlide.is(':animated') && !$nextSlide.is(':animated')) {
                this._isSliding = false;
                this.$currentSlide.removeClass('active').hide();
                this.$currentSlide = $nextSlide;
                this.$currentSlide.addClass('active');
            }
        }.bind(this));
    };
    
    /**
     * Показывает предыдущий слайд (по аналогии с показом следующего)
     */
    Slider.prototype.showPrev = function () {
    
        var $prevSlide = this.$currentSlide.prev().length ? this.$currentSlide.prev() : this.ui.slides.last();
        var $moveGroup = _.clone(this.$currentSlide);
    
        $moveGroup.push($prevSlide[0]);
    
        $prevSlide.css({
            left: parseInt(this.$currentSlide.css('left')) - _size.width
        }).show();
    
        this._isSliding = true;
    
        $moveGroup.animate({
            left: '+=' + _size.width
        }, 200, function () {
            if (!this.$currentSlide.is(':animated') && !$prevSlide.is(':animated')) {
                this._isSliding = false;
                this.$currentSlide.removeClass('active').hide();
                this.$currentSlide = $prevSlide;
                this.$currentSlide.addClass('active');
            }
        }.bind(this));
    };
    
    /**
     * Выравнивает элементы слайдера.
     * Используется, когда меняются размеры окна, слайда и т.д.
     */
    Slider.prototype.showViewport = function () {
        var $prevSlide = this.$currentSlide.prev();
        var $nextSlide = this.$currentSlide.next();
    
        if ($prevSlide.length) {
            $prevSlide.css({
                left: -_size.width
            });
        }
    
        if ($nextSlide.length) {
            $nextSlide.css({
                left: +_size.width
            });
        }
    
        this.$currentSlide.css({
            left: 0
        });
    };
    
    /**
     * Размеры окна поменялись
     */
    Slider.prototype.onWindowResize = function () {
        _size.width = $(window).width() - sideOffset * 2;
        _size.height = $(window).height() - sideOffset * 2;
    
        // если попап есть в доме, значит надо редактировать размеры слайдов
        if (typeof this.ui !== 'undefined') {
    
            this.showViewport();
    
            this.ui.container.width(_size.width);
            this.ui.container.height(_size.height);
    
            this.ui.slides.find('img').each(function (index, image) {
    
                var $image = $(image);
                var size = proportionalSize($image.height(), $image.width());
    
                if (size.height > _size.height) {
                    $image.height('100%');
                    $image.width('auto');
                } else {
                    $image.width('100%');
                    $image.height('auto');
                }
    
            });
        }
    };

    $.fn.slider = function() {

        var slider = new Slider(this);

        $(window).resize(_.debounce(function () {
            Slider.prototype.onWindowResize.call(slider, arguments);
        }, 500));

        return slider;
    };

}));