
var gulp = require('gulp');
var runSequence = require('run-sequence');

var sass = require('gulp-sass');

var rigger = require('gulp-rigger');

gulp.task('css', function () {

    return gulp.src('src/css/slider.scss')
        .pipe(sass())
        .pipe(gulp.dest('dist'));
});

gulp.task('js', function () {

    return gulp.src('src/js/slider.js')
        .pipe(rigger())
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', function () {

    gulp.watch('src/css/**/*.scss', ['css']);

    gulp.watch('src/js/**/*.js', ['js']);
});

gulp.task('default', function (callback) {

    runSequence('css', 'js', callback);
});