function Slider(element) {

    this.$el = element;

    this.images = [];

    this.$currentSlide = null;
    this._isSliding = false;

    this.init();
}